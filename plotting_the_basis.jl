using LightGraphs

import GraphPlot
import Compose:cm, SVG, PDF
import Gadfly
import Colors
import Cairo
import Fontconfig
import Plots

begin
    N = 32
    g = random_regular_graph(N, 3)
    node = 10
    name = "rrg"
end

begin
    g = grid([10,10])
    node = 65
    name = "grid"
end


A = adjacency_matrix(g)

fixed_layout = GraphPlot.spring_layout(g, MAXITER=1000)
layout_func = (x) -> fixed_layout

for n in 0:6


    nfc = Array{Float64}((A^n)[node,:])
    nfc ./= maximum(nfc)
    nfc .*= 0.9
    nfc .+= 0.1

    cs = [Colors.RGBA(1. - c,0.8,0.8, 1.) for c in nfc]
    cs[node] = Colors.RGBA(1.,0.,1.,1.)

    net_plot = GraphPlot.gplot(g, layout=layout_func, nodefillc=cs)

    Gadfly.draw(PDF("$(name)_$n.pdf", 16cm, 16cm), net_plot)
end
